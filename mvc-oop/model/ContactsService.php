<?php

require_once 'model/Gateway.php';
require_once 'model/ValidationException.php';

class ContactsService extends Gateway{
    public $table = "contacts";
    
    public function __construct() {
        parent::__construct($this->table);
    }
    
    public function getAllContacts() {
        try {
            $res = $this->selectAll();
            return $res;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function getContact($id) {
        try {
            $arrPost = [
                'id' => $id
            ];
            $res = $this->selectById($arrPost);
            return $res;
        } catch (Exception $e) {
            throw $e;
        }
        return $this->find($id);
    }
    
   
    public function createNewContact( $name, $phone, $email, $address ) {
        try {
            $arrPost = [
                'name' => $name,
                'phone' => $phone,
                'email' => $email,
                'address' => $address,
            ];
            $res = $this->insert($arrPost);

            return $res;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public function deleteContact( $id ) {
        try {
            $arrPost = [
                'id' => $id
            ];
            $res = $this->delete($arrPost);
        } catch (Exception $e) {
            throw $e;
        }
    }
}

?>
