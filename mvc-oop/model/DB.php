<?php
class DB{
    private $contactsGateway    = NULL;
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "contacts";
    public $conn;
    
    public function __construct() {
        $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);                
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->conn=$conn;
        return $this;
    }
    
    public function closeDb() {
        $conn=null;
    }
}
