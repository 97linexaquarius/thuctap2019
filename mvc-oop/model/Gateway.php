<?php
require_once 'model/DB.php';
//Model
class PDOnew extends DB{
    public $table='';
    public $conn;
    public $whereOption='';
    public $select='';
    public $orderCd='';

    public function __construct() {
        parent::__construct();
    }
    public function table($table){
        $this->table=$table;
        return $this;
    }
    public function select(){
        $numArgs = func_num_args();
        if($numArgs>0){
            $arg_list = func_get_args();
            $this->select=implode(", ",$arg_list);
        }
        return $this;
    }
    public function orderBy($field='id',$order='ASC'){
        $this->orderCd.=($this->orderCd=="")? " $field $order ":" ,$field $order ";
        return $this;
    }
    //khi mở rộng sẽ extends pdonew->where sau đó check lại số agument nếu bằng 2 sẽ gọi parent::where không thì define hàm mới 
//    public function where($field,$value){
//        $numArgs = func_num_args();
//        //print_r( $numArgs);
//        if($numArgs>=3){
//            $arg_list = func_get_args();
//            $this->whereOption=implode("",$arg_list);
//        }else{
//            $this->whereOption=" $field=$value ";
//        }
//        return $this;
//    }
//    public function orWhere($field,$value){
//        $numArgs = func_num_args();
//        //print_r( $numArgs);
//        if($numArgs>=3){
//            $arg_list = func_get_args();
//            $this->whereOption.=" OR ".implode("",$arg_list). " ";
//        }else{
//            $this->whereOption.=" OR $field=$value ";
//        }
//        return $this;
//    }
//    public function andWhere($field,$value){
//        $this->whereOption.=" AND $field=$value ";
//        return $this;
//    }
    public function where($field,$operator = '=',$value){
        return $this->whereLogicOperator('', $field, $operator, $value);
    }
    public function orWhere($field,$operator = '=',$value){
        return $this->whereLogicOperator(' or ', $field, $operator, $value);
    }
    public function andWhere($field,$operator = '=',$value){
        return $this->whereLogicOperator(' and ', $field, $operator, $value);
    }
    public function whereLogicOperator($logicOperator, $field, $operator = '=', $value){
        $numArgs = func_num_args();
        if($numArgs>=4){
            $arg_list = func_get_args();
            $this->whereOption.=implode(" ", $arg_list);
        } else if($numArgs>=3){
            $this->whereOption.=" $field $operator $value ";
        } else {
            $this->whereOption.=" $field $operator $value ";
        }

        return $this;
    }

    public function getAll(){
        echo "SELECT "
            .($this->select==''?'*':$this->select)." FROM $this->table "
            .($this->whereOption!=''?" Where $this->whereOption ":'')
            .($this->orderCd!=''?" ORDER By $this->orderCd ":'')
        ;
        $query = $this->conn->prepare("SELECT "
            .($this->select==''?'*':$this->select)." FROM $this->table "
            .($this->whereOption!=''?" Where $this->whereOption ":'')
            .($this->orderCd!=''?" ORDER By $this->orderCd ":'')
        );
        $query->execute();
        $students = array();
        while ( ($obj = $query->fetch(PDO::FETCH_OBJ)) != NULL ) {
            $students[] = $obj;
        }
        return $students;
    }
}

//My old code
class Gateway  {
    public $soft_delete = 'deleted';
    public $table;
    public function __construct($table) {
        $this->DB = new DB();
        $this->table = $table;
    }
    public function soft_delete(){
        return "$this->soft_delete=false";
    }
    public function selectAll() {
        $conn = $this->DB->openDb();
        $query = $conn->prepare("SELECT * FROM $this->table WHERE ".$this->soft_delete());
        $query->execute();
        $contacts = array();
        while ( ($obj = $query->fetch(PDO::FETCH_OBJ)) != NULL ) {
            $contacts[] = $obj;
        }
        $this->DB->closeDb();

        return $contacts;
    }
    
    public function selectById($arrPost) {
        $conn = $this->DB->openDb();
        $key = array_keys($arrPost)[0];
        $value = $arrPost[$key];
        $query = $conn->prepare("SELECT * FROM $this->table WHERE $key = $value");

        //must fix bind $value to prevent sql injection
        $query->execute();
        $this->DB->closeDb();

        return $query->fetch(PDO::FETCH_OBJ);
    }
    
    public function insert( $arrPost ) {
        $conn = $this->DB->openDb();
        $fields = $values = array();
        foreach( array_keys($arrPost) as $key ) {
            $fields[] = "`$key`";
            $values[] = "'" . $arrPost[$key] . "'";
        }
        $fields = implode(",", $fields);
        $values = implode(",", $values);
        
        $query = $conn->prepare("INSERT INTO $this->table ($fields) VALUES ($values)");

        //must fix bind $value to prevent sql injection

        $this->DB->closeDb();

         $query->execute();
         return $lastId = $conn->lastInsertId();
    }
    
    public function delete($arrPost) {
        $conn = $this->DB->openDb();
        $key = array_keys($arrPost)[0];
        $value = $arrPost[$key];
        $query = $conn->prepare("UPDATE $this->table SET $this->soft_delete=true WHERE $key = $value");

        //must fix bind $value to prevent sql injection

        $this->DB->closeDb();

        return $query->execute();
    }
}

?>
