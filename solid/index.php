<?php
//Liskov substitution principle
interface extraActivities{
    function runForClassPresident();
}
//End Liskov substitution principle
//Interface – Interface segregation
interface CommonStudy
{
    function studyEnglish();
    function studyMath();
}

interface AdvancedStudy
{
    function studyFrench();
}

interface ForeignStudy
{
    function studyPhilosophy();
}
//End Interface – Interface segregation
//Single responsibility principle
class Student
{
    public $name;
    public $age;
    public $schoolFee = 1000000;
    public function __construct($name, $age){
        $this->name = $name;
        $this->age = $age;
    }
    function getName()
    {
        return $this->name;
    }
 
    function getAge()
    {
        return $this->age;
    }
    
    function getStudentInfo(){
        $studentInfo = new Formatter();
        return $studentInfo->getStudentInfoHtml($this->getName(), $this->getAge());
    }

    function applyForScholarship()
    {
        try
        {
            // do something here
        }
        catch (Exception $e)
        {
            $log = new ExportLog();
            return $log->exportLogToFile("c://log/log.txt", $e->getMessage());
        }
    }
    function paySchoolFee()
    {
        return $this->schoolFee;
    }
}
 
class Formatter
{
    function formatInfoJson($name, $age)
    {
        return json_encode( array( $name, $age ) );
    }
 
    function getStudentInfoHtml($name, $age)
    {
        return "<span> Tên: " . $name . ", Tuổi: " . $age . "</span>";
    }
}
 
class ExportLog
{
    function exportLogToFile($filename, $error)
    {
        echo "Đã ghi lỗi!";
    }
}
//End Single responsibility principle

//Open/closed principle
class NormalStudent extends Student implements extraActivities, CommonStudy {
    function paySchoolFee()
    {
        return $this->schoolFee;
    }
    function runForClassPresident()
    {
        // TODO: Implement runForClassPresident() method.
        echo "Hợp lệ";
    }

    function studyEnglish()
    {
        // TODO: Implement studyEnglish() method.
    }

    function studyMath()
    {
        // TODO: Implement studyMath() method.
    }
}
class AdvancedStudent extends Student implements extraActivities, CommonStudy, AdvancedStudy
{
    function paySchoolFee()
    {
        return $this->schoolFee * 0.8;
    }

    function runForClassPresident()
    {
        // TODO: Implement runForClassPresident() method.
        echo "Hợp lệ";
    }

    function studyEnglish()
    {
        // TODO: Implement studyEnglish() method.
    }

    function studyMath()
    {
        // TODO: Implement studyMath() method.
    }

    function studyFrench()
    {
        // TODO: Implement studyFrench() method.
    }
}
 
class ForeignStudent extends Student implements CommonStudy, ForeignStudy
{
    public $visa;
    function __construct($name, $age, $visa){
        parent::__construct($name, $age);
        $this->visa = $visa;
    }
    function paySchoolFee()
    {
        return $this->schoolFee * 1.3;
    }
    function showVisa(){
        return $this->visa;
    }

    function studyEnglish()
    {
        // TODO: Implement studyEnglish() method.
    }

    function studyMath()
    {
        // TODO: Implement studyMath() method.
    }

    function studyPhilosophy()
    {
        // TODO: Implement studyPhilosophy() method.
    }
}
//End Open/closed principle
$student = new Student('Hoàng Anh', 20);
echo $student->getStudentInfo() . '<br>';
echo 'Học phí: '.$student->paySchoolFee().' VNĐ <br>';

$normalStudent = new NormalStudent('Lê Thành', 20);
echo 'Học phí: '.$normalStudent->paySchoolFee().' VNĐ <br>';
$advancedStudent = new AdvancedStudent('Nguyễn Nam', 20);
echo 'Học phí cao học: '.$advancedStudent->paySchoolFee().' VNĐ <br>';
$foreignStudent = new ForeignStudent('Alex Ferguson', 19, 'Chưa hết hạn');
echo 'Visa: '.$foreignStudent->showVisa().'<br>';

$myArray[] = $normalStudent;
$myArray[] = $advancedStudent;
$myArray[] = $foreignStudent;
foreach($myArray as $key=>$value){
    if ($value instanceof extraActivities) {
        echo $value->runForClassPresident();
    }
}





