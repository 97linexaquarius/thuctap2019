$(document).ready(function () {
	var imenu = [];
	var idElement;
	$("ul.treeview-menu .box").draggable({
		helper: 'clone',
		// connectToSortable: "#content .row"
	});
	$("#content").droppable({
		accept: "ul.treeview-menu .box",
		drop: function (e, ui) {
			var item = $(this).append($(ui.draggable).clone());
			$("#content .box").remove();
			element = $(event.target).attr('class').split(/\s+/)[1];
			if (element != null) {
				draw(item, element);

			}

		}
	});
	function draw(item, element) {

		$.getJSON("dist/js/json/" + element + ".json", function (data) {

			// var row = document.createElement("div");
			// row.className += "row";
			for (var i in data) {
				imenu.push(data[i]);

				var title = document.createElement(data[i].tag);
				title.className += data[i].class;
				title.className += " item";

				if (data[i].hasOwnProperty('inside')) {

					for (var j in data[i].inside) {

						var node = document.createElement(data[i].inside[j].tag);

						if (data[i].inside[j].hasOwnProperty('content')) {


							var textnode = document.createTextNode(data[i].inside[j].content);
							node.appendChild(textnode);

						}
						if (data[i].inside[j].hasOwnProperty('attr')) {

							$.each(data[i].inside[j].attr, function (key, value) {
								var a = document.createAttribute(key);
								a.value = value;
								node.setAttributeNode(a);
							});
						}

						title.append(node);
					}
				}
				// row.append(title);
				item.append(title);
			}
			// console.log(imenu);
		});

	}
	$("#content").sortable({
		start: function (e, ui) {
			window.itemSort = imenu.splice(ui.item.index(), 1);
		},
		stop: function (e, ui) {
			imenu.splice(ui.item.index(), 0, window.itemSort[0]);
		}
	});


	/*
		Coding change content
	*/
	// $('.item').click(function(){
	// 	console.log(1);
	// 	alert( $('.item').index(this) );
	// });


	// <div class="form-group">
	//               <label>Email address</label>
	//               <input type="text" class="form-control">
	//             </div>
	$('body').on('click', "[class*=' item']", function (e) {
		$(".box-body").empty();
		idElement = $(this).index();
		console.log(idElement);
		console.log(imenu[idElement]);
		$.each(imenu[idElement].inside, function (key, value) {
			if (imenu[idElement].inside[key].hasOwnProperty('attr')) {
				$.each(imenu[idElement].inside[key].attr, function (k, value) {
					$('<label>' + k + '</label>').appendTo('.box-body');
					var input = document.createElement("input");
					input.type = "text";
					
					input.className = "form-control parent-"+key+" attr-"+k; // set the CSS class
					input.value = value;
					$('.box-body').append(input);
				});
			}
			if (imenu[idElement].inside[key].hasOwnProperty('content')) {
				$('<label>value</label>').appendTo('.box-body');
				var value = imenu[idElement].inside[key].content;
				var input = document.createElement("input");
				input.type = "text";
				input.className = "form-control parent-"+key+" attr-content"; // set the CSS class
				input.value = value;
				$('.box-body').append(input);
			}
			var foo = "<hr/>";
			$('.box-body').append(foo);
		});

		
	});
	$('.change-content').click(function (e) { 
		e.preventDefault();
		console.log(imenu[idElement]);
	});
});