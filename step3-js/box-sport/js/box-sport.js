//li width:230px + 15px:margin-left
const liWidth = 178;
const liMarginLeft = 15;
var fruits = [0];
var selector = "ul.gallery";
var length = countListItem(selector)-1;
var current = 0;
var ulWidth = 574;
var numberItem = 3;
function countListItem(selector){
    var countListItem = $(selector+' li').length;
    return countListItem;
}
$(document).ready(function () {
    for(let i=1; i<(length/numberItem); i++){
        fruits.push(-(liWidth+liMarginLeft)*i*numberItem);
    }
    fruits[fruits.length-1] = -((liWidth+liMarginLeft)*countListItem(selector)-ulWidth);
    console.log(fruits);
    $(".btn-next").click(function (e) { 
        e.preventDefault();
        if(current<length/numberItem-1){
            current++
            $(selector).css("transform", `translateX(${fruits[current]}px)`);
        }
    });
    $(".btn-prev").click(function (e) { 
        e.preventDefault();
        if(current>0){
            current--;
            $(selector).css("transform", `translateX(${fruits[current]}px)`);
        }  
    });
    
});