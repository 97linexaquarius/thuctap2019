//li width:230px + 15px:margin-left
const liWidth = 200;
const liMarginLeft = 15;
var fruits = [0];
var selector = "ul.news-list";
var length = countListItem(selector)-1;
var current = 0;
var ulWidth = 550;
var numberItem = 2;
function countListItem(selector){
    var countListItem = $(selector+' li').length;
    return countListItem;
}
$(document).ready(function () {
    $( ".wrap .content .pagination" ).append( "<span class='bullet' id='0'></span>" );
    for(let i=1; i<(length/numberItem); i++){
        fruits.push(-(liWidth+liMarginLeft)*i*numberItem);
        $( ".wrap .content .pagination" ).append( "<span class='bullet' id='"+i+"'></span>" );
    }
    $( ".bullet" ).first().addClass('active');
    fruits[fruits.length-1] = -((liWidth+liMarginLeft)*countListItem(selector)-ulWidth);
    console.log(fruits);
    $(".button-next").click(function (e) { 
        e.preventDefault();
        if(current<length/numberItem-1){
            current++
            $(".bullet.active").removeClass( "active" );
            $(selector).css("transform", `translateX(${fruits[current]}px)`);
            $(".bullet#"+(current)).addClass( "active" );
        }
    });
    $(".button-prev").click(function (e) { 
        e.preventDefault();
        if(current>0){
            current--;
            $(".bullet.active").removeClass( "active" );
            $(selector).css("transform", `translateX(${fruits[current]}px)`);
            $(".bullet#"+current).addClass( "active" );
        }  
    });
    $(".pagination").click(function (e) { 
        e.preventDefault();
        if(e.target.attributes.id.value !== 'undefined') {
            current = e.target.attributes.id.value;
            console.log("Current ="+current);
            $(".bullet.active").removeClass( "active" );
            $(".bullet#"+e.target.attributes.id.value).addClass( "active" );
            $(selector).css("transform", `translateX(${fruits[current]}px)`);
        }
    });
});