var fruits = [0];
var selector = "ul.news-list";
var length = countListItem(selector)-1;
var current = 0;
var liHeigh = 149;

function countListItem(selector){
    var countListItem = $(selector+' li').length;
    return countListItem;
}
$(document).ready(function () {
    $("#c").append((current+1)+"/"+(length+1));

    for(let i=0; i<length; i++){
        fruits.push(-liHeigh*(i+1));
    }
    $('.controlbox .up').click(function (e) { 
        e.preventDefault();
        if(current<length){
            $(selector).css("transform", `translateY(${fruits[current+1]}px)`);
            current++;
            $("#c").html('');
            $("#c").append((current+1)+"/"+(length+1));
        }
    });
    $('.controlbox .down').click(function (e) { 
        e.preventDefault();
        if(current>0){
            current--;
            $(selector).css("transform", `translateY(${fruits[current]}px)`);
            $("#c").html('');
            $("#c").append((current+1)+"/"+(length+1));
        }  
    });
});