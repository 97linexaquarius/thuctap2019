<?php
class Database{
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "contacts";

    protected $pdo;
    public function __construct()
    {
        $pdo = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo=$pdo;
        return $this;
    }
    public final function execute($sql)
    {
        return $this->pdo->exec($sql);
    }
    public function query($sql, array $params = [])
    {
        if (! empty($params)) {
            $sth = $this->pdo->prepare($sql);
            $sth->execute($params);
            return $sth;
        }

        return $this->pdo->exec($sql);
    }
}
?>