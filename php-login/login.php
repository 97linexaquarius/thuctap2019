<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?php
        require 'DB.php';
        //login
        if (isset($_POST['email']) & isset($_POST['password'])){
            $mail = $_POST['email'];
            $pass = $_POST['password'];
            try {
                $query = $conn->prepare("SELECT email FROM users WHERE  email=:email AND password=:password");
                $query->bindParam("email", $mail);
                $enc_password = hash('sha256', $pass);
                $query->bindParam("password", $enc_password);
                $query->execute();
                if ($query->rowCount() > 0) {
                    echo "Đăng nhập thành công";
                } else {
                    echo "Thông tin tài khoản không đúng";
                }
            } catch (PDOException $e) {
                exit($e->getMessage());
            }
            $conn = null;
        }
    ?>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <h1>Đăng nhập</h1>
                <form method="post" id="loginForm">
                    <div class="form-group">
                        <label for="">Địa chỉ email</label>
                        <input type="email" name="email" class="form-control" id="email" required>
                    </div>
                    <div class="form-group">
                        <label for="">Mật khẩu</label>
                        <input type="password" name="password" minlength="6" class="form-control" id="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Đăng nhập</button>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
    <script>
        $(document).ready(function () {
            $("#signupForm").validate({
                rules:{
                    repass:{
                        equalTo: "#pass"
                    }
                }
            });
            $("#loginForm").validate();
        });
    </script>
</body>
</html>