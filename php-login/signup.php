<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <?php
        require 'DB.php';
        //signup
        if (isset($_POST['mail']) & isset($_POST['pass']) & isset($_POST['repass'])){
            $mail = $_POST['mail'];
            if($_POST['pass']==$_POST['repass']){
                $pass = $_POST['pass'];
            }
            try {
                //check exist
                $sqlExist = "SELECT COUNT(email) as mail FROM users WHERE email = (:email)";
                $stmt = $conn->prepare($sqlExist);
                $stmt->bindParam(':email', $mail);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if($row['mail'] > 0){
                    die('Tài khoản đã tồn tại');
                }
                $stmt = $conn->prepare("INSERT INTO users (email, password) 
                VALUES (:email, :password)");
                $stmt->bindParam(':email', $mail);
                $enc_password = hash('sha256', $pass);
                $stmt->bindParam(':password', $enc_password);
                $stmt->execute();
                echo "Thêm thành công";
                }
            catch(PDOException $e)
                {
                echo $e->getMessage();
                }
            $conn = null;
        }
    ?>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <h1>Đăng ký</h1>
                <form method="post" id="signupForm">
                    <div class="form-group">
                        <label for="">Địa chỉ email</label>
                        <input type="email" name="mail" class="form-control" id="mail" required>
                    </div>
                    <div class="form-group">
                        <label for="">Mật khẩu</label>
                        <input type="password" name="pass" minlength="6" class="form-control" id="pass" required>
                    </div>
                    <div class="form-group">
                        <label for="">Nhập lại mật khẩu</label>
                        <input type="password" name="repass" minlength="6" class="form-control" id="repass" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Đăng ký</button>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
    <script>
        $(document).ready(function () {
            $("#signupForm").validate({
                rules:{
                    repass:{
                        equalTo: "#pass"
                    }
                }
            });
            $("#loginForm").validate();
        });
    </script>
</body>
</html>